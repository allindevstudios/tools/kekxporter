## KRA.JS

- findAllKritaFilesInFolderSync

## FILE-HELPERS

- sortFilesInFolders -> also find better name
    the idea is that a JSON with folder and then branching key-values for files are done

    so sortedFiles = {
        folder1: {
            files: {

            }
        }
        folder2: {
            folder3: {
                files: {

                }
            }
        }
        files: {

        }
    }

- findAllFilesInFolderSync -> allow for level depth definition


## FUTURE NOTES

- Look at how to fix the sidebar and scalable auto height adjustments.
- Look at other frameworks...

- Make a good layout on paper of how things should work
- Make a simple page which just bulk exports everything
    this has like 4 settings -> Krita path, from and to and autocrop