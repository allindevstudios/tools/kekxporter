import _ from 'lodash';
import { findAllFilesInFolderSync } from 'file-helpers';


function sortFilesInFolder(folderPath) {
  const folderContents = findAllFilesInFolderSync(folderPath)
    .map(file => file.replace(`${folderPath}/`, ''));

  const files = folderContents.filter(file => file.indexOf('/') === -1);
  const folders = _.uniq(folderContents
    .filter(file => file.indexOf('/') !== -1)
    .map(folder => folder.split('/')[0]));

  const sortedFolders = folders.map(folder => sortFilesInFolder(`${folderPath}/${folder}`));

  return {
    name: folderPath.split('/').pop(),
    files,
    folders: sortedFolders,
  };
}

function sortFilesToElementUITree({ files, folders }) {
  const fileTreeElements = files.map(file => Object({
    label: file,
  }));
  const folderTreeElements = folders.map(folder => Object({
    label: folder.name,
    children: sortFilesToElementUITree(folder),
  }));
  return folderTreeElements.concat(fileTreeElements);
}

const files = sortFilesToElementUITree(sortFilesInFolder('/home/laurent/Downloads'));

export default {
  data() {
    return {
      files,
      defaultProps: {
        children: 'children',
        label: 'label',
      },
    };
  },
  methods: {
    fileClicked(file) {
      console.log(file);
    },
  },
};
