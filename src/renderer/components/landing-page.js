import FileTree from './ProjectManager/FileTree';
import SideBar from './ProjectManager/SideBar';

// const data = findAllFilesInFolderSync('/home/laurent/Downloads/');
// console.log(data);

export default {
  components: {
    FileTree,
    SideBar,
  },
};
